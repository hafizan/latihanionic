import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = "https://reqres.in/"

  constructor(private http: HttpClient) { }

  getUserList(): Observable<any>{
    return this.http.get(this.baseUrl+"api/users");
  }

  getUserById(userId: number): Observable<any>{
    return this.http.get(this.baseUrl+"api/users/"+userId);
  }

}
