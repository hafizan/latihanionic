import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  data: any;

  constructor() { }

  setData(x){
    this.data = x;
  }

  getData(){
    return this.data;
  }

}
