import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../model/user';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  userList: User[] = new Array<User>();

  constructor(
    private storage: Storage
  ) {}

  ngOnInit() {
    this.storage.forEach((value, key) =>{
      //add current data into list
      this.userList.push(value);
    });

    console.log(this.userList);
  }

  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
