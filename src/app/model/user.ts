export class User {
    public name: string;
    public email: string;
    public sex: string;
    public password: string;

    public setName(name: string){
        this.name = name;
    }

    public setEmail(email: string){
        this.email = email;
    }

    public setSex(sex: string){
        this.sex = sex;
    }

    public setPassword(password: string){
        this.password = password;
    }
}