import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController, NavController } from '@ionic/angular';
import { User } from '../model/user';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  userForm: FormGroup;
  user: User = new User();
  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: Storage,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.initForm()
  }

  initForm(){
    this.userForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(15)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      sex: 'M'
    });
  }

  onRegister(){
    //get value from user input
    var fullnameIn = this.userForm.controls['name'].value;
    var emailIn = this.userForm.controls['email'].value;
    var passwordIn = this.userForm.controls['password'].value;
    var sexIn = this.userForm.controls['sex'].value;

    //get form status (VALID/INVALID)
    var formStatus = this.userForm.status;

    if(formStatus == "VALID"){
      //set input into user object
      this.user.setName(fullnameIn);
      this.user.setPassword(passwordIn);
      this.user.setSex(sexIn);
      this.user.setEmail(emailIn);

      console.log(this.user);
      //save into storage
      this.storage.set(emailIn, this.user);

      //alert
      this.presentToast();

      //redirect to login
      this.goToLogin();
    }

  }

  goToLogin(){
    this.router.navigateByUrl("/login");
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your data has been successfully submitted',
      duration: 2000,
      color: "primary",
      position: "bottom"
    });
    toast.present();
  }

}
