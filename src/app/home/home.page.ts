import { Component } from '@angular/core';
import { UtilService } from '../services/util.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  welcomeTxt: any;

  constructor(private _util: UtilService) {
    this.welcomeTxt = this._util.getData();
  }

}
