import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnInit {

  userList: any[];

  constructor(
    private _user: UserService, 
    private router: Router
    ) { }

  ngOnInit() {
    this._user.getUserList().subscribe(
      data => {
        this.userList = data.data;
      }
    );
  }

  profile(id){
    this.router.navigateByUrl("/user-profile/"+id);
  }

}
