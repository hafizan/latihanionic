import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilService } from '../services/util.service';
import { Storage } from '@ionic/storage';
import { User } from '../model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  welcomeTxt: string = "Sign Up";
  loginForm: FormGroup;
  isShow: boolean = true;
  statusForm: boolean = true;
  userList: User[] = new Array<User>();

  constructor(
    public toastController: ToastController,
    private formBuilder: FormBuilder,
    private router: Router,
    private _util: UtilService,
    private storage: Storage
    ) {
  }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    //initialize form
    this.loginForm = this.formBuilder.group({
      username: '',
      password: ''
    });
  }

  onSubmit(){

    //get value from login form
    var username = this.loginForm.controls['username'].value;
    var password = this.loginForm.controls['password'].value;

    //business logic
    if(this.checkIfUserExist(username, password)){
      this._util.setData(username);
      this.router.navigateByUrl("/home");
    }else{
      this.presentToast();
    }
  }

  checkIfUserExist(username, password){
    //load user from storage
    this.storage.forEach((value, key) => {
      this.userList.push(value);
    });

    for(let user of this.userList){
      if((user.email == username) && (user.password == password)){
        return true
      }
    }

    return false;
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Incorrect username/password',
      duration: 2000,
      color: "danger",
      position: "bottom"
    });
    toast.present();
  }

  test(){
    if(this.isShow == true){
      this.isShow = false;
    }else if(this.isShow == false){
      this.isShow = true;
    }
  }

  goToRegister(){
    this.router.navigateByUrl("register");
  }

}
