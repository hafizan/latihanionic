import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  userForm: FormGroup;
  id: any;
  avatar: any;

  constructor(
    private formBuilder:FormBuilder,
    private _user: UserService,
    private actRoute: ActivatedRoute
    ) {
      //get parameter id from url
      this.id = this.actRoute.snapshot.paramMap.get("id");
  }

  ngOnInit() {
    this.initForm();
    this.loadUserProfile();
  }

  loadUserProfile(){
    //load profile
    this._user.getUserById(this.id).subscribe(
      data => {
        //set user data into form
        this.userForm.patchValue({
          fname: data.data.first_name,
          lname: data.data.last_name,
          email: data.data.email
        });
        this.avatar = data.data.avatar;
      }
    );
  }

  initForm(){
    this.userForm = this.formBuilder.group({
      fname:'',
      lname:'',
      email:''
    });
  }

}
